#' resolve values
#'
#' @param row_name the corresponding focovana in the analysis matrix
#' @param col the col
#' @param mapping a data frame with process variable mappings
#' @param colname name of the column of the matrix
#' @param col2function the mapping of colnames to function names
#' @param df_missingtable the data frame of the missing list
#' 
#' @author Stephan Struckmann, Jörg Henke
#' 
#' @return list of lists
util_resolve_values_element <- function(row_name, col, mapping, 
                                        colname, col2function, df_missingtable){
  arguments <- col[[row_name]]
  stopifnot("resp_vars" %in% names(arguments))
  resp_vars <- arguments[["resp_vars"]]
  arguments[["resp_vars"]] <- NULL
  # TODO: get argument names
  result <- lapply(setNames(nm = names(arguments)), util_resolve_value, 
                   resp_vars = resp_vars, 
                   mapping = mapping, colname = colname, 
                   row_name = row_name,
                   arguments = arguments,
                   col2function = col2function,
                   df_missingtable = df_missingtable)
  result[["resp_vars"]] <- resp_vars
  return(result)
}
