#' fetch the analysis matrix with its arguments from the database
#'
#' @param con the database connection
#' @param report_id the id of the report
#'
#' @author Stephan Struckmann, Jörg Henke
#' 
#' @importFrom jsonlite fromJSON toJSON validate
#'
#' @return a matrix with the arguments
#' @export
#' 
db_fetch_matrix <- function(con, report_id) {

  df_matrixelementparam <- dbxSelect(con, SQL3, report_id)
  df_matrixcolumnparam <- dbxSelect(con, SQL4, report_id)
  df_matrixparentparam <- dbxSelect(con, SQL5, report_id)
  
  # see https://gitlab.com/umg_hgw/sq2/square2/-/issues/912
  replace_json_object <- function(v_origin, colname, value){
    v_corrected <- unlist(lapply(v_origin, function(x){
      if (validate(x)){
        json_orig <- fromJSON(x)
        json_orig[[colname]] = value
        json_corrected <- toJSON(json_orig, auto_unbox = TRUE) 
        return(json_corrected)
      } else {
        return(x)
      }
    }))
    return(v_corrected)
  }
  
  fixEmptyCovars <- function(df_unfixed){
    df_fixed <- df_unfixed
    if (nrow(df_fixed) > 0){
      cond <- (is.na(df_fixed$check_value) | df_fixed$check_value == "null") & 
        df_fixed$input_name == "co_vars" # TODO: Do so for all inputs of type variable -- this may fis https://gitlab.com/umg_hgw/sc/implementation/sq2psqlrepository/-/issues/26
      if (nrow(df_fixed[cond, ]) > 0) {
        df_fixed[cond, ]$value <- replace_json_object(
          df_fixed[cond, ]$value, "refers_metadata", FALSE)
      }
    }
    return(df_fixed)
  }
  
  r_matrixelementparam <- fixEmptyCovars(df_matrixelementparam)
  r_matrixcolumnparam <- fixEmptyCovars(df_matrixcolumnparam)
  r_matrixparentparam <- fixEmptyCovars(df_matrixparentparam)

  function_alias_map <- dbxSelect(con, SQL6, list(report_id))
  
  if (nrow(function_alias_map) < 1){
    stop("no functions found in report")
  }
  
  function_category_map <- dbxSelect(con, SQL7, list(report_id))
  
  r_matrixfunctionparam <- dbxSelect(con, SQL8, list(function_alias_map$fk_function))
  
  df_multivariatcolumns <- dbxSelect(con, SQL9, list(report_id))
  
  the_matrix <- dbxSelect(con, SQL10, report_id)
  
  df_confounder <- dbxSelect(con, SQL11, report_id)
  
  df_col_indices <- dbxSelect(con, SQL12, report_id) 
    
  df_row_indices <- dbxSelect(con, SQL13, report_id) 
  
  df_confounder[["focovana"]] <- unique_name_to_focovana(df_confounder[["unique_name"]])
  df_confounder[["referenced_focovana"]] <- unique_name_to_focovana(df_confounder[["referenced_unique_name"]])
  
  m <- matrix("{}", 
              nrow = length(unique(the_matrix$unique_name)), 
              ncol = length(unique(the_matrix$alias)),
              dimnames = list(unique(the_matrix$unique_name),
                           unique(the_matrix$alias)))
  m_cols <- colnames(m)
  alias_map <- setNames(function_alias_map$fk_function, 
                        nm = function_alias_map$alias)
  m_fktnid <- alias_map[m_cols]
  
  r_matrixfunctionparam$value <- util_typerestriction2value(r_matrixfunctionparam)
  
  df_matfunparam <- 
    r_matrixfunctionparam[!is.na(r_matrixfunctionparam$value) & 
                              trimws(r_matrixfunctionparam$value) != "", , 
                              drop = FALSE]
  if (nrow(df_matfunparam) > 0) {
    stopifnot(all(vapply(df_matfunparam$value, util_is_valid_json,
                         FUN.VALUE = logical(1))))
    df_matfunparam$param <- paste0('"', df_matfunparam$input_name, '"',
                                   ": ",
                                   df_matfunparam$value)
    mfp <- tapply(df_matfunparam$param,
                  df_matfunparam$fk_function,
                 paste, collapse = ", ")
   
    fktns <- df_matfunparam$fk_function
    fktns <- fktns[!duplicated(fktns)]
  
    df_mfp <- data.frame(fk_function = fktns,
                         params = mfp[as.character(fktns)])
    
    df_mfp$json_params <- paste0("{", df_mfp$params, "}")
    df_mfp$params <- NULL
    
    fktnparam_map <- setNames(df_mfp$json_params,
                              nm = df_mfp$fk_function)
  
    m[, m_cols] <- t(matrix(fktnparam_map[as.character(m_fktnid)], 
                            ncol = nrow(m),
                            nrow = length(m_cols)))
    
  }
  
  for (df_name in c("r_matrixparentparam", "r_matrixcolumnparam")) {
    
    df_currentmatrixparam <- get(df_name)
    
    df_currentmatrixparam$value <- util_typerestriction2value(df_currentmatrixparam)
  
    df_matcolparam <- 
      df_currentmatrixparam[!is.na(df_currentmatrixparam$value) & 
                              trimws(df_currentmatrixparam$value) != "", , 
                              drop = FALSE]
    if (nrow(df_matcolparam) > 0){
      stopifnot(all(vapply(df_matcolparam$value, util_is_valid_json, 
                           FUN.VALUE = logical(1))))
      df_matcolparam$param <- paste0('"', df_matcolparam$input_name, '"',
                                     ": ",
                                     df_matcolparam$value)
      mcp <- tapply(df_matcolparam$param,
                    df_matcolparam$alias,
                    paste, collapse = ", ")
      
      aliases <- df_matcolparam$alias
      aliases <- aliases[!duplicated(aliases)]
      
      df_mcp <- data.frame(aliases = aliases,
                           params = mcp[aliases])
      df_mcp$json_params <- paste0("{", df_mcp$params, "}")
      df_mcp$params <- NULL
  
      
      mcparam_map <- setNames(df_mcp$json_params,
                              nm = df_mcp$aliases)
      
      mcp_aliases <- unique(df_currentmatrixparam$alias)
      m_cols_mcp <- m_cols[m_cols %in% mcp_aliases]
  
      m[, m_cols_mcp] <- t(matrix(mcparam_map[m_cols_mcp], 
                                  ncol = nrow(m),
                                  nrow = length(m_cols_mcp)))
    } 
  }

  r_matrixelementparam$value <- util_typerestriction2value(r_matrixelementparam)

  df_mateleparam <- 
    r_matrixelementparam[!is.na(r_matrixelementparam$value) & 
                              trimws(r_matrixelementparam$value) != "", , 
                            drop = FALSE]
  if (nrow(df_mateleparam) > 0){
    stopifnot(all(vapply(df_mateleparam$value, util_is_valid_json, 
                         FUN.VALUE = logical(1))))
    df_mateleparam$param <- paste0('"', df_mateleparam$input_name, '"',
                                   ": ",
                                   df_mateleparam$value)
    mep <- t(tapply(df_mateleparam$param,
                    list(df_mateleparam$alias, df_mateleparam$unique_name),
                    paste, collapse = ", "))
    mep[!is.na(mep)] <- paste0("{", mep[!is.na(mep)], "}")
    
    for (i in colnames(mep)) {
      .mep <- mep[, i, drop = FALSE]
      .mob <- .mep[!is.na(.mep), , drop = FALSE]
      m[rownames(.mob), i] <- .mob[, i]
    }
  }

  # support functions without arguments
  m[is.na(m)] <- "{}"
  
  the_nulls <- the_matrix
  
  checkmarks <- t(tapply(the_nulls$checked,
                  list(the_nulls$alias, the_nulls$unique_name),
                  identity))

  for (i in colnames(checkmarks)) {
    .checkcol <- checkmarks[, i, drop = FALSE]
    .mob <- .checkcol[!.checkcol, , drop = FALSE]
    m[rownames(.mob), i] <- NA_character_
  }

  if (nrow(df_multivariatcolumns) > 0){
    attr(m, "multivariatcol") <- df_multivariatcolumns$name
  } else {
    attr(m, "multivariatcol") <- character(0)
  }
  
  attr(m, "col2function") <- setNames(function_alias_map$name, 
                                      nm = function_alias_map$alias)
  
  attr(m, "function_alias_map") <- function_alias_map
  
  attr(m, "function2category") <- setNames(function_category_map$category,
                                           nm = function_category_map$name)
  
  attr(m, "confounder") <- df_confounder
  
  attr(m, "col_indices") <- setNames(nm = df_col_indices$name, df_col_indices$order_nr)
  attr(m, "row_indices") <- setNames(nm = df_row_indices$unique_name, df_row_indices$varorder)

  return(m)
}
