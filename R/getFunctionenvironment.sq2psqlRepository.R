#' get the function environment
#'
#' @param repository the repository
#'
#' @return the function environment
#' @export
#' @importFrom squarerepositoryspec getFunctionenvironment
getFunctionenvironment.sq2psqlRepository <- function(repository){
  return(repository$functionenvironment)
}