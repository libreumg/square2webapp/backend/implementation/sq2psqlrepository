#' get sendMsgCalcStarted
#'
#' @param repository the object of the implementation
#' 
#' @importFrom squarerepositoryspec sendMsgCalcStarted
#'
#' @export
sendMsgCalcStarted.sq2psqlRepository <- function(repository){
  sendMsg(repository, STARTED_CALCULATION, Sys.time())
}
