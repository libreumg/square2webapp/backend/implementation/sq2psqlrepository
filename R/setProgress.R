#' set the progress of the currently computed report
#' 
#' @param repository the object of the implementation
#' @param progress_msg message
#' @param progress_pct percentage in `[0; 1]`
#' @importFrom squarerepositoryspec setProgress
#' @export
setProgress.sq2psqlRepository <- function(repository, 
                                          progress_msg, 
                                          progress_pct) {
  rop <- util_normalize_path(findReportFolder(repository$output_path, 
                                        report_id = repository$report_id), 
                       mustWork = CREATE_DIR)
  
  # TODO add the log file to rop
  # TODO ensure that rop exists in all other functions relying on rop, too
  if (!dir.exists(rop)) {
    if (file.exists(rop)) {
      stop(sprintf("%s is a file, not a folder!", rop))
    }
    dir.create(rop, recursive = TRUE)
  }
  if (!dir.exists(rop)) {
    stop(sprintf("could not create folder %s", rop))
  }
  
  key <- getReportKey.sq2psqlRepository(repository)
  if (!file.exists(file.path(rop, "report_key")))
  cat(key,
      file = file.path(rop, "report_key"))
  
  if (!missing(progress_msg)) {
    if (is.na(progress_msg)) {
      unlink(file.path(rop, "progress_msg"))
    } else {
      cat(progress_msg,
          file = file.path(rop, "progress_msg"))
    }
  }
  if (!missing(progress_pct)) {
    if (is.na(progress_pct)) {
      unlink(file.path(rop, "progress_pct"))
    } else {
      cat(progress_pct,
          file = file.path(rop, "progress_pct"))
    }
  }
}
