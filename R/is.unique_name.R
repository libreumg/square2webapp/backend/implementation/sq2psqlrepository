#' check, if the candidate is a unique_name
#'
#' @param candidate the unique_name
#'
#' @return true if candidate is a unique_name
#' @export
is.unique_name = function(candidate){
  grepl("^[A-Za-z][A-Za-z0-9]*\\.[A-Za-z][A-Za-z0-9_]*$", candidate)
}