#' get the missing table for the variable group
#'
#' @param repository the object of the implementation
#' @param con the database connection, if available; deprecated
#'
#' @author Jörg Henke
#'
#' @return the missing table, at least an empty one
#' @importFrom squarerepositoryspec getMissingtable
#' 
#' @export
#' @examples
#' \dontrun{
#' library(sq2psqlRepository)
#' library(squarerepositoryspec)
#' repo <- sq2psqlRepository(config_section = "test", report_id = 445)
#' View(getMissingtable(repository = repo))
#' }
getMissingtable.sq2psqlRepository <- function(repository, con) {
  futile.logger::flog.info("entering getMissingtable.sq2psqlRepository")
  df_missinglist <- repository[["df_missinglist"]]
  if (is.null(df_missinglist)){
    df_missinglist <- run_dbx(config = repository$config_filename, 
                              section = repository$config_section, 
                              function(con){
                                db_require_version(75, con, ignore_check = repository$.mock)
                                # TODO: replace repository$report_id by getID(repository) or such
                                df_missinglist <- db_load_missinglist(con, repository$report_id)
                                return(df_missinglist)
                              })
    repository[["df_missinglist"]] <- df_missinglist
  }
  futile.logger::flog.info("leaving getMissingtable.sq2psqlRepository")
  return(df_missinglist)
}