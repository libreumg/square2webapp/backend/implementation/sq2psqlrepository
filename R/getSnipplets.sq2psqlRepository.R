#' get snipplets from the database
#'
#' @param repository the repository
#'
#' @return the loaded snipplets or NULL if not yet found
#'
#' @author Jörg Henke
#' @export
#' @importFrom squarerepositoryspec getSnipplets
#' @importFrom squaredbconnector db_require_version
#'
getSnipplets.sq2psqlRepository <- function(repository) {
  futile.logger::flog.info("entering getSnipplets.sq2psqlRepository")
  dfs <- run_dbx(config = repository$config_filename, 
                 section = repository$config_section, 
                 function(con){
    db_require_version(68, con, ignore_check = repository$.mock)
    df_report <- dbxSelect(con, SQL26, list(repository$report_id))
    
    # TODO: select * from square.r_reportproperty -- already in another interface (getReportProperties)
    # TODO: r_snipplet.chunk_options JSONB
    df_snipplet <- dbxSelect(con, SQL27, list(repository$report_id))
    
    props <- c(name = df_report$name, description = df_report$description)
    # TODO: extend props by report properties
    
    return(snipplets = df_snipplet)
  })
  futile.logger::flog.info("leaving getSnipplets.sq2psqlRepository")
  return(dfs)
}
