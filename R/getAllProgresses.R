#' get all the progresses for a given repositories
#'
#' @param ... currently, only the argument `output_path` is supported and 
#'   needed, telling the function where to search for `sq2psalRepository`
#'   progress files.
#'
#' @export
#'
getAllProgresses <- function(...) {
  args <- list(...)
  stopifnot("output_path" %in% names(args))
  output_path <- args[[output_path]]
  
  progress_files <- c(
    list.files(output_path,
               pattern = "report_key",
               recursive = TRUE, 
               full.names = TRUE
              ),
    list.files(output_path,
               pattern = "progress_(msg|pct)",
               recursive = TRUE, 
               full.names = TRUE
    )
  )
  roots <- unique(dirname(progress_files))
  all_progresses <- lapply(
    roots,
    function(root) {
      report_key <- NA_character_
      progress_msg <- NA_character_
      progress_pct <- NA_character_
      if (file.exists(file.path(root, "report_key")))
        report_key <- readLines(file.path(root, "report_key"))
      if (file.exists(file.path(root, "progress_msg")))
        progress_msg <- readLines(file.path(root, "progress_msg"))
      if (file.exists(file.path(root, "progress_pct")))
        progress_pct <- readLines(file.path(root, "progress_pct"))
      cancelled <- file.exists(file.path(root, "cancel_request"))
      data.frame(
        report_key = report_key,
        progress_msg = progress_msg,
        progress_pct = progress_pct,
        cancelled = cancelled,
        stringsAsFactors = FALSE)
    }
  )
  do.call(rbind.data.frame, all_progresses)
}
