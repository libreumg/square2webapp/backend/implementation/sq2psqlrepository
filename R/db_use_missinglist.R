#' get the missing list ids from the database
#'
#' @param con the database connection
#' @param missinglist the missinglist content
#'
#' @return a vector with missinglist ids
#' @author Jörg Henke
#' @importFrom dbx dbxSelect dbxUpsert
db_use_missinglist <- function(con, missinglist) {
  # TODO: select pk from square.t_missinglist
  # if missinglist does not exist, create one
  # return the pks as a vector, keep the order!
  # missings from the field missinglist in the dataquieR format are always of category missing, not jump
  return(NA)
}