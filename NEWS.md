# sq2psqlRepository 1.2.10
  - found solution for missing lists and its functions

# sq2psqlRepository 1.2.9
  - remove deprecated code

# sq2psqlRepository 1.2.8
  - make missing codes dataquieR compatible

# sq2psqlRepository 1.2.7
  - repair constant name of levels

# sq2psqlRepository 1.2.6
  - repair missing list category column name (CODE_CLASS)

# sq2psqlRepository 1.2.5
  - force compatibility to dataquieR 1.0.9.9006

# sq2psqlRepository 1.2.4
  - support dataquieR compatible missing lists

# sq2psqlRepository 1.2.3
  - removed unix dependency from description file

# sq2psqlRepository 1.2.2
  - deactivated dependency to os dependent package unix

# sq2psqlRepository 1.2.1
  - use R view due to privileges

# sq2psqlRepository 1.2.0
  - support function to get all reports of a user

# sq2psqlRepository 1.1.24
  - switch from r_report to t_report (omit refactored relicts)

# sq2psqlRepository 1.1.23
  - corrected progress percent number

# sq2psqlRepository 1.1.22
  - prevent lintr from crashing
  
# sq2psqlRepository 1.1.21

  - as mentioned in issue #30, report base directories were not always created. 
    This has been fixed by introducing a new utility function working as a
    drop-in replacement for `normalizePath`.

# sq2psqlRepository 1.1.20

  - fixed "folder not found" on writing reports

# sq2psqlRepository 1.1.19

  - fixed "folder does not exist" on calculation start

# sq2psqlRepository 1.1.18

  - added CI/CD
