
<!-- README.md is generated from README.Rmd. Please edit that file -->

# `sq2psqlRepository`

<!-- badges: start -->
<!-- badges: end -->

The goal of `sq2psqlRepository` is to provide access to all data about a
QA report directly from the `Square2` database. Alternatives are e.g. to
fetch the metadata from `Opal` (`opalRepository`) or uploaded files
(`sq2filerepository`)

The package implements the interface defined in `squarerepositoryspec`.
