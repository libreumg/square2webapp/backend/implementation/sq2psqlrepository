% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/writeReportFiles.R
\name{writeReportFiles.sq2psqlRepository}
\alias{writeReportFiles.sq2psqlRepository}
\title{write a report document with dependent files}
\usage{
\method{writeReportFiles}{sq2psqlRepository}(repository, render_args, file)
}
\arguments{
\item{repository}{the repository object}

\item{render_args}{arguments used for rendering the report to distinguish
slightly different report artifacts. The only used argument should be the
\code{format} for now, anything else should be removed from the rendering call,
so that the \code{format} should be enough to distinguish different report-file
directories.}

\item{file}{the contents to write as a list of raw vectors, named by
the file or directory names, e.g. \verb{list(dir = list(afile = raw(10)), }
\verb{fileb = raw(10))} for \verb{/dir/afile} and \verb{/fileb}}
}
\value{
undefined
}
\description{
write a report document with dependent files
}
