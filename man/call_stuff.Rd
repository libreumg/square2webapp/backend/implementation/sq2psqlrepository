% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/call_stuff.R
\name{call_stuff}
\alias{call_stuff}
\title{call_stuff}
\usage{
call_stuff(what, args)
}
\arguments{
\item{what}{function to be called}

\item{args}{arguments for the function as list}
}
\description{
call_stuff
}
\examples{
\dontrun{
source('~/git/gitlab/sq2psqlRepository/R/call_stuff.R', echo=TRUE)
load(system.file("extdata", "study_data.RData", package = "dataquieR"))
call_stuff(dataquieR::acc_distributions, list(study_data = study_data))
}
}
