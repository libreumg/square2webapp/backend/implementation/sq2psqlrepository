% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/focovana_to_unique_name.R
\name{focovana_to_unique_name}
\alias{focovana_to_unique_name}
\title{convert focovanas to unique_name}
\usage{
focovana_to_unique_name(focovana)
}
\arguments{
\item{focovana}{the focovana (may be a vector)}
}
\value{
the converted unique_name
}
\description{
convert focovanas to unique_name
}
