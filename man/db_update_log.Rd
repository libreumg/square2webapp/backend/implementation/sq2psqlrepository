% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/db_update_log.R
\name{db_update_log}
\alias{db_update_log}
\title{update the column \code{runlog} in \code{t_report}}
\usage{
db_update_log(con, report_id, logmsg)
}
\arguments{
\item{con}{the \code{dbx} database connection}

\item{report_id}{the id of the report}

\item{logmsg}{the process ID}
}
\value{
nothing
}
\description{
update the column \code{runlog} in \code{t_report}
}
\author{
Jörg Henke
}
