% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/getMissingtable.sq2psqlRepository.R
\name{getMissingtable.sq2psqlRepository}
\alias{getMissingtable.sq2psqlRepository}
\title{get the missing table for the variable group}
\usage{
\method{getMissingtable}{sq2psqlRepository}(repository, con)
}
\arguments{
\item{repository}{the object of the implementation}

\item{con}{the database connection, if available; deprecated}
}
\value{
the missing table, at least an empty one
}
\description{
get the missing table for the variable group
}
\examples{
\dontrun{
library(sq2psqlRepository)
library(squarerepositoryspec)
repo <- sq2psqlRepository(config_section = "test", report_id = 445)
View(getMissingtable(repository = repo))
}
}
\author{
Jörg Henke
}
